import sys
weight = int(input("How heavy is your package? "))
method = input("normal, premium,drone or all? ")

if weight <= 2:
  ground_cost = 1.5 * weight + 20
elif weight <= 6:
  ground_cost = 3 * weight + 20
elif weight <= 10:
  ground_cost = 4 * weight + 20
else:
  ground_cost = 4.75 * weight + 20


premium_ground_cost = 125

if weight <= 2:
  drone_cost = weight * 4.5
elif weight <= 6:
  drone_cost = weight * 9.00
elif weight <= 10:
  drone_cost = weight * 12.00
else:
  drone_cost = weight * 14.25

if method == 'normal':
    print("Cost of sending with ground: ", ground_cost)
elif method == 'premium':
    print("Cost of sending with Premium ground: ", premium_ground_cost)
elif method == 'drone':
    print("Cost of sending package with drone: ", drone_cost)
else:
    print("Cost of sending with Premium ground: ", premium_ground_cost)
    print("Cost of sending with ground: ", ground_cost)
    print("Cost of sending package with drone: ", drone_cost)