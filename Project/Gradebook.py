last_semester_gradebook = [["politics", 80], ["latin", 96], ["dance", 97], ["architecture", 65]]

# Your code below: 
# Creates list of grades and subjects
subjects = ["pysics", "calculus", "poetry", "history"]
grades = [98, 97, 85, 88]
gradebook = [list(z) for z in zip(subjects, grades)] #add 2 lists together
#add 2 new subjects
gradebook.append(["computer science", 100]) 
gradebook.append(["visual arts", 93])
#Changes grades on arts and poetry
gradebook[-1][-1] = 98
gradebook[2].remove(85)
gradebook[2].append("Pass")
#Adds 2 semesters togheter and Print it out
full_gradebook = last_semester_gradebook + gradebook
print("My grades for last 2 semester is", full_gradebook)
